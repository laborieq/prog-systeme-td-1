#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

int main(int argc, char **argv) {
    int in = 0, out = 1;
    //Récupération des paramètres
    if (argc > 1) {
        in = open(argv[1], O_RDONLY);
    }
    if (argc > 2) {
        out = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0644);
    }
    //Lecture écriture
    char buf[4096];
    int nb_read;
    do {
        nb_read = read(in, buf, 4096);
        write(out, buf, nb_read);
    } while (nb_read > 0);
}